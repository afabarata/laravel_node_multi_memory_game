// Require HTTP module (to start server) and Socket.IO
var http = require('http');
var io = require('socket.io');
var gameEngine = require('./game');
var port = 8080;

//start http
var server = http.createServer(function(req, res){
});

server.listen(port);
console.log('Server listening on port ' + port);

// -------------------------------------------------
// Web Socket --------------------------------------
// -------------------------------------------------

function CurrentPlayer(id,nickname) {
    this.id = id;
    this.nickname = nickname;
};

var games= [];
var playersOnGame = [];
var playerCounter = 0;
var currentPlayer = [];
var isEnd = false;

var io = io.listen(server, {
    log: false,
    agent: false,
    origins: '*:*'
});

io.on('connection', function(socket){
    console.log('\n----------------------------------------------------\n');
    console.log('Connection to client established');

    socket.on('startGame',function(playerId,playerName,gameId,cols,lines,maxPlayers){
        console.log('\n----------------------------------------------------\n');
        console.log('Client with id ' + playerId +' requested "startGame" - gameId = ' + gameId);

        socket.join(gameId);

        games[gameId] = gameEngine.gameStart(cols, lines, maxPlayers);
        playersOnGame = gameEngine.playerJoin(playersOnGame,playerId,playerName);

        currentPlayer = new CurrentPlayer(playerId, playerName);
        playerCounter = gameEngine.playerCount(playersOnGame);

        io.in(gameId).emit('refreshGame', games[gameId], playersOnGame, currentPlayer, playerCounter,isEnd);
        console.log('CurrentPlayer: ' + currentPlayer);
        console.log('PlayerOnGame ', playersOnGame);
        console.log('Games ', games);
    });

    socket.on('joinGame',function(playerId,playerName,gameId){
        console.log('\n----------------------------------------------------\n');
        console.log('Client with id ' + playerId +' requested "joinGame" - gameId = ' + gameId);

        gameEngine.playerJoin(playersOnGame,playerId,playerName);
        socket.join(gameId);

        playerCounter = gameEngine.playerCount(playersOnGame);

        io.in(gameId).emit('refreshGame',games[gameId],playersOnGame, currentPlayer,playerCounter,isEnd);
        console.log('CurrentPlayer: ' + currentPlayer);
        console.log('PlayerOnGame ', playersOnGame);
        console.log('Games ', games);
    });

    socket.on('watchGame',function(playerId,gameId){
        console.log('\n----------------------------------------------------\n');
        console.log('Client with id ' + playerId +' requested "watchGame" - gameId = ' + gameId);

        socket.join(gameId);

        io.in(gameId).emit('refreshGame',games[gameId],playersOnGame, currentPlayer, playerCounter,isEnd);
    });

    socket.on('playMove',function(playerId, gameId, games ){
        console.log('\n----------------------------------------------------\n');
        console.log('Client with id ' + playerId +' requested "playMove" - gameId = ' + gameId);

        currentPlayer = gameEngine.nextPlayer(playersOnGame,currentPlayer);
        gameEngine.playMove(games,playersOnGame);

        isEnd = gameEngine.endGame(games);
        console.log('The Game is Over: ' + isEnd);

        io.in(gameId).emit('refreshGame', games,playersOnGame, currentPlayer, playerCounter,isEnd);
        console.log('CurrentPlayer: ' + currentPlayer);
        console.log('Games ', games);
    });

    //CHAT ON GAME LOBBY

    socket.on('msgChatAll',function(playerName, chatMsg){
        console.log('\n----------------------------------------------------\n');
        console.log('Client send a chat message = ' + playerName+ ' - '+chatMsg);

        io.emit('newChatMsg', playerName, chatMsg);
    });

    //CHAT ON GAME

    socket.on('msgChatGame',function(gameId, playerName, chatMsg){
        console.log('\n----------------------------------------------------\n');
        console.log('Client send a chat message = "' + chatMsg + '" to room ' + gameId);

        io.in(gameId).emit('newChatMsg', playerName, chatMsg);
    });

    socket.on('disconnect',function(){
        console.log('\n----------------------------------------------------\n');
        console.log('Disconnect');
    });


});