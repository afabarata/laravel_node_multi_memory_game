var maxPlayers = 0;
var status = false;

var z = 1;
var isFirstTimeTimer = true;
var isFirstTimeTries = true;
var isFirstTimePairs = true;
var nextPlayerAux = 1;
var timer = null;

function Piece(id) {
    this.id = id;
    this.flipped = false;
    this.hidden = false;
};

function Player(id,nickname) {
    this.id = id;
    this.nickname = nickname;
    this.timer = 0;
    this.tries = 0;
    this.pairs = 0;
};

function CurrentPlayer(id,nickname) {
    this.id = id;
    this.nickname = nickname;
};

function playerCounter(obj) {
    var result = 0;
    for(var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            result++;
        }
    }
    return result;
};

function isGameFull(playersOnGame) {

    var length = playerCounter(playersOnGame);

    if( length < maxPlayers )
    {
        return false;
    } else {
        return true;
    }
};

function isGameEnd(games) {

    for(var i = 0; i < games.length; i++) {
        var game = games[i];
        for(var j = 0; j < game.length; j++) {
            if(game[j].hidden == false) {
                return false;
            }
        }
    }
    return true;
};

module.exports = {

    playerJoin: function(playersOnGame,playerId,playerName){

        if(!isGameFull(playersOnGame)) {
            playersOnGame.push(new Player(playerId,playerName));
        }

        return playersOnGame;
    },
    gameStart: function(cols, lines, maxPlayer){

        maxPlayers = maxPlayer;

        var game = buildBoard(cols, lines);

        function buildBoard(cols, lines) {

            var numCols = cols;
            var numLines = lines;
            var tiles = [];
            var num = 0;

            var arrayVal = [];
            var finalArray = [];

            for(var i=0; i < (numCols*numLines)/2 ; i++) {
                arrayVal[i] = Math.floor((Math.random() * 40) + 1);
            }

            finalArray = arrayVal.concat(arrayVal);
            shuffle(finalArray);

            for (var row = 0; row < numCols; row++) {
                tiles[row] = [];
                for (var col = 0; col < numLines; col++) {
                    tiles[row][col] = new Piece(finalArray[num]);
                    num++;
                }
            }

            return tiles;
        };

        function shuffle(array) {
            var currentIndex = array.length, temporaryValue, randomIndex ;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        };

        return game;
    },
    playerCount: function (playersOnGame){
        var counter;

        counter = playerCounter(playersOnGame);

        return counter;
    },
    nextPlayer: function (playersOnGame,currentPlayer){

        var length = playerCounter(playersOnGame);

        if(length == 1) {
            z= 0;
        }else {
            if(nextPlayerAux == 2) {

                currentPlayer = new CurrentPlayer(playersOnGame[z].id,playersOnGame[z].nickname);
                z++;

                if(z == length){
                    z = 0;
                }

                nextPlayerAux = 1;

                return currentPlayer;
            } else {
                nextPlayerAux++;
            }
        }

        return currentPlayer;
    },
    playMove: function(games,playersOnGame) {

        var firstTile = null;
        var secondTile = null;

        timer = setInterval(function () {
            if(isFirstTimeTimer) {
                playersOnGame[0].timer++;
            } else {
                playersOnGame[z].timer++;
            }
        }, 1000);

        for (var i = 0; i < games.length; i++) {
            var game = games[i];
            for (var j = 0; j < game.length; j++) {
                if (game[j].flipped == true) {
                    if (firstTile == null) {
                        firstTile = game[j];
                    } else {
                        secondTile = game[j];
                    }
                }
            }
        }

        if (firstTile != null && secondTile != null) {

            if (firstTile.id == secondTile.id) {

                firstTile.hidden = true;
                secondTile.hidden = true;
                firstTile = null;
                secondTile = null;

                if(isFirstTimePairs) {
                    playersOnGame[0].pairs++;
                } else {
                    playersOnGame[z].pairs++;
                }

            } else {
                firstTile.flipped = false;
                secondTile.flipped = false;
                firstTile = null;
                secondTile = null;
            }

            if(isFirstTimeTries) {
                playersOnGame[0].tries++;
            } else {
                playersOnGame[z].tries++;
            }

            isFirstTimeTimer = false;
            isFirstTimeTries = false;
            isFirstTimePairs = false;

            clearInterval(timer);
        }

        return games;

    },
    endGame: function (game) {
        status = isGameEnd(game);

        return status;
    }


};