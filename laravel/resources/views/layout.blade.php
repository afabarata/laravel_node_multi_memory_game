<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>Memory Game</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ URL::asset('libs/bootstrap-3.3.6-dist/css/bootstrap.min.css')}}">

    <!-- jquery ui css -->
    <link rel="stylesheet" href="{{ URL::asset('libs/jquery-ui-1.11.4/jquery-ui.css')}}">

    <!-- flippy ui css -->
    <link rel="stylesheet" href="{{ URL::asset('libs/flippy/flippy.css')}}">

    <!-- rzslider ui css -->
    <link rel="stylesheet" href="{{ URL::asset('libs/rzslider/rzslider.css')}}">

    <!-- datatable css -->
    <link rel="stylesheet" href="{{ URL::asset('libs/DataTables-1.10.10/media/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('libs/DataTables-1.10.10/extensions/Responsive/css/responsive.dataTables.min.css')}}">

    <!-- alertify css -->
    <link rel="stylesheet" href="{{ URL::asset('libs/alertifyjs/css/alertify.css')}}">

    <!-- font awewsome -->
    <link rel="stylesheet" href="{{ URL::asset('libs/font-awesome-4.5.0/css/font-awesome.css')}}">

    <!-- personal css -->
    @yield('css')



    <!-- jQuery library -->
    <script src="{{ URL::asset('libs/jquery-1.11.3.min.js')}}"></script>

    <!-- jquery ui compiled JavaScript -->
    <script src="{{ URL::asset('libs/jquery-ui-1.11.4/jquery-ui.min.js')}}"></script>

    <!-- angular js compiled JavaScript -->
    <script src="{{ URL::asset('libs/angular.min.js')}}"></script>

    <!-- socket.io js compiled JavaScript -->
    <script src="{{ URL::asset('libs/socket.io-1.2.0.js')}}"></script>

    <!-- flippy js compiled JavaScript -->
    <script src="{{ URL::asset('libs/flippy/flippy.min.js')}}"></script>

    <!-- rzslider js compiled JavaScript -->
    <script src="{{ URL::asset('libs/rzslider/rzslider.js')}}"></script>

    <!-- Latest compiled JavaScript -->
    <script src="{{ URL::asset('libs/bootstrap-3.3.6-dist/js/bootstrap.min.js')}}"></script>

    <!-- datatable js -->
    <script src="{{ URL::asset('libs/DataTables-1.10.10/media/js/jquery.dataTables.min.js')}}"></script>

    <!-- alertify js -->
    <script src="{{ URL::asset('libs/alertifyjs/alertify.js')}}"></script>

    <!-- personal js -->
    @yield('js')

</head>
    <body>
        @yield('content')
    </body>
</html>