@extends('layout')

@section('css')
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/styles.css')}}">
@stop

@section('js')
    <script src="{{ URL::asset('assets/js/gameCreation.js')}}"></script>
@stop

@section('content')
<div class="container" id="gameCreation">
    <div class="col-md-6 col-md-offset-3">
        <form method="post" action="/gameCreation">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Game Creation</h3>
                </div>
                <div class="panel-body">
                    <input class="hidden" name="id_user" id="id_user" value="{{ Auth::user()->id }}">
                    <div class="form-group">
                        <label for="name">Game Name</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Game Name" value="{{ old('name') }}" >
                    </div>
                    <div class="form-group">
                        <label for="private">Private Game</label>
                        <label class="radio-inline"><input type="radio" name="private" id="privateYes" value="{{ 1 }}">Yes</label>
                        <label class="radio-inline"><input type="radio" name="private" id="privateNo" value="{{ 0 }}" checked>No</label>
                    </div>
                    <div class="form-group" id="maxPlayer">
                        <label for="num_max_players">Max Players</label>
                        <input type="number" class="form-control" name="num_max_players" id="num_max_players" value="{{ old('num_max_players') }}" >
                    </div>
                    <div class="form-group">
                        <label for="cols">Cols</label>
                        <input type="number" class="form-control" name="cols" id="cols" value="{{ old('cols') }}" readonly>
                    </div>
                    <div id="sliderCols"></div>
                    <div class="form-group">
                        <label for="lines">Lines</label>
                        <input type="number" class="form-control" name="lines" id="lines" value="{{ old('lines') }}" readonly>
                    </div>
                    <div id="sliderLines"></div>
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
                <div class="panel-footer">
                    <div class="row footerBtn">
                        <div class="pull-left">
                            <a type="button" href="{{route('gameLobby')}}"class="btn btn-danger">Cancel</a>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success">Create</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop