@extends('layout')

@section('css')
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/styles.css')}}">
@stop

@section('js')
    <script src="{{ URL::asset('assets/js/gameController.js')}}"></script>
@stop

@section('content')
<div class="container" id="game" ng-app="memoryGame" ng-controller="GameController" @if($status == "watch") style="pointer-events: none" @endif>
    <div class="col-sm-10 col-sm-offset-1">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <input type="hidden" id="idGame" name="gameId" value="{{ $game->id }}" ng-model="gameId">
                <input type="hidden" id="idPlayer" name="playerId" value="{{ Auth::user()->id }}" ng-model="playerId">
                <input type="hidden" id="maxPlayers" name="maxPlayers" value="{{ $game->num_max_players }}" ng-model="playerId">
                <h3 class="panel-title">{{ $game->name }}</h3>
            </div>
            <div class="panel-body">
                <div class="col-sm-5">
                    <h4 class="title">Definitions</h4>
                    <input type="hidden" id="cols" value="{{ $game->cols }}">
                    <input type="hidden" id="lines" value="{{ $game->lines }}">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="current_num_players">Players</label>
                            <input type="number" class="form-control" name="current_num_players" id="num_max_players" value="@{{ current_players }}" readonly>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="current_num_players">Board Size</label>
                            <input type="text" class="form-control" name="board_size" id="board_size" value="{{ $game->cols }} x {{ $game->lines }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-sm-7">
                    <h4 class="title">Global</h4>
                    <table id="playersTable">
                        <tr>
                            <th>Timer</th>
                            <th>Times Played</th>
                            <th>Total Pairs</th>
                            <th>Remaining Tiles</th>
                        </tr>
                        <tr>
                            <td>0 sec</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                        </tr>
                    </table>
                    <h4 class="title">Players</h4>
                    <table id="playersTable">
                        <tr>
                            <th>Nickname</th>
                            <th>Timer</th>
                            <th>Times Played</th>
                            <th>Total Pairs</th>
                        </tr>
                        <tr ng-repeat="player in playerList">
                            <td>@{{ player.nickname }}</td>
                            <td>@{{ player.timer }} sec</td>
                            <td>@{{ player.tries }}</td>
                            <td>@{{ player.pairs }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-12">
                    <hr>
                    <h4 class="title">Game</h4>
                    <span id="currentPlayer">Now is @{{ currentPlayerName }} turn!</span>
                    <hr>
                </div>

                <div class="col-sm-12" id="gameBoard" ng-if="currentPlayerId == {{ Auth::user()->id }}">
                    <table>
                        <tbody>
                            <tr ng-repeat="row in tiles">
                                <td  ng-repeat="tile in row">
                                    <div class="card" ng-hide="tile.hidden">
                                        <flippy
                                                ng-class="{flipped: tile.flipped}"
                                                ng-click="touch(tile)"
                                                flip-duration="800"
                                                timing-function="ease-in-out">
                                            <flippy-front>
                                                <img src="{{ URL::asset('assets/gameImages/hidden.png') }}" alt="the front" />
                                            </flippy-front>
                                            <flippy-back>
                                                <img ng-src="{{ URL::asset('assets/gameImages') }}/@{{tile.id}}.png" alt="the back" />
                                            </flippy-back>
                                        </flippy>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-sm-12" id="gameBoard" ng-if="currentPlayerId != {{ Auth::user()->id }}" style="pointer-events: none">
                    <table>
                        <tbody>
                        <tr ng-repeat="row in tiles">
                            <td  ng-repeat="tile in row">
                                <div class="card" ng-hide="tile.hidden">
                                    <flippy
                                            ng-class="{flipped: tile.flipped}"
                                            ng-click="touch(tile)"
                                            flip-duration="800"
                                            timing-function="ease-in-out">
                                        <flippy-front>
                                            <img src="{{ URL::asset('assets/gameImages/hidden.png') }}" alt="the front" />
                                        </flippy-front>
                                        <flippy-back>
                                            <img ng-src="{{ URL::asset('assets/gameImages') }}/@{{tile.id}}.png" alt="the back" />
                                        </flippy-back>
                                    </flippy>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="panel-footer">
                <div class="row footerBtn">
                    <div class="pull-right">
                        @if($game->id_user == Auth::user()->id)
                            <a type="button" class="btn btn-success pull-right" ng-click="startGame()"> Start Game </a>
                        @endif
                        @if($game->id_user != Auth::user()->id)
                            @if($status == "join")
                                <a type="button" class="btn btn-info pull-right" ng-click="joinGame()"> Join Game </a>
                            @elseif($status == "watch")
                                <a type="button" class="btn btn-warning pull-right" ng-click="watchGame()"> Watch Game </a>
                            @endif
                        @endif
                    </div>
                    <div class="pull-left">
                        <a type="button" id="closeGame" class="btn btn-danger" href="{{route('gameLobby',['id'=>$game->id])}}">Return to Lobby</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary" id="chatPanel">
            <div class="panel-heading">
                <h3 class="panel-title">Chat</h3>
            </div>
            <div class="panel-body">
                <input type="hidden" id="playerName" name="playerName" value="{{ Auth::user()->nickname }}">
                <div id="messages">
                    <span ng-repeat="m in chatMessages track by $index">@{{ m }}</span>
                </div>
                <form action="#" method="get" id="id_form_chat">
                    <input class="form-control" id="m" autocomplete="off"  ng-model="chatMsg" ng-keypress="keyPressMsg($event)">
                </form>
            </div>
        </div>

    </div>
    <div style="display:none">
        <div id="finalGameContent">
            <h4>The Winner is:</h4>
            <br>
            <form method="post" action="/game">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="name">Player Nickname</label>
                    <input type="text" class="form-control" name="nickname" id="nickname" value="@{{ winner.nickname }}" readonly>
                </div>
                <div class="form-group">
                    <label for="name">Number of Pairs</label>
                    <input type="text" class="form-control" name="pairs" id="pairs" value="@{{ winner.pairs }}" readonly>
                </div>
                <input type="hidden" name="gameId" id="gameId" value="{{ $game->id }}">
                <br>
                <div class="pull-right">
                    @if($game->id_user == Auth::user()->id)
                        <button type="submit" class="btn btn-success">Return To Lobby</button>
                    @else
                        <a type="button" class="btn btn-success" href="{{route('gameLobby')}}" >Return To Lobby</a>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
@stop