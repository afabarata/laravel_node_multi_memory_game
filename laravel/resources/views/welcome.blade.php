@extends('layout')

@section('css')
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/css/index.css')}}">
@stop

@section('content')
<div class="container" id="welcomePage">
    <div class="content">
        <div class="title">
            <p>Welcome to Memory Game</p>
        </div>
        <div class="options">
            <?php if (Auth::check()): ?>
                <a id="gameLobby" href="{{route('gameLobby')}}">Game Lobby</a>
                <a id="logout" href="{{route('logout')}}" >Logout</a>
            <?php else: ?>
                <a id="guest" href="" >Enter as Guest</a>
                <a id="login" href="{{route('login')}}" >Login</a>
                <a id="register" href="{{route('register')}}" >Register</a>
            <?php endif; ?>

        </div>
        <div class="authors">
            <p>Designed and Programmed by André Barata & Paulo Penicheiro</p>
        </div>
        <div class="uc">
            <p>to Desenvolvimento de Aplicações Distribuidas of ESTG Computer Engineering </p>
        </div>
    </div>
</div>
@stop
