@extends('layout')

@section('css')
    <link rel="stylesheet" href="{{ URL::asset('assets/css/styles.css')}}">
@stop

@section('js')
    <script src="{{ URL::asset('assets/js/gameLobby.js')}}"></script>
@stop

@section('content')
<div class="container" id="gameLobby" ng-app="memoryGame" ng-controller="LobbyController">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                Game Lobby
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#">&nbsp;</a></li>
                    <li><a href="{{route('home')}}">Back to Welcome Page</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{route('gameCreation')}}" target="_blank">Create Game</a></li>
                    <li><a href="{{route('gameLobby')}}">Refresh</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    <div class="content" id="lobby">

        <div class="col-md-8">

            <div class="panel panel-primary" id="openGamesPanel">
                <div class="panel-heading">
                    <h3 class="panel-title">Enter Game</h3>
                </div>
                <div class="panel-body">
                    <table id="openGames" class="display" width="100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Max Players</th>
                            <th>Size</th>
                            <th>Created At</th>
                            <th>Admin</th>
                            <th>Enter</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($games_open as $game_open)
                            <tr>
                                <td>{{ $game_open->name }}</td>
                                <td>{{ $game_open->num_max_players }}</td>
                                <td>{{ $game_open->cols }} x {{ $game_open->lines }}</td>
                                <td>{{ $game_open->created_at }}</td>
                                <td></td>
                                <td><a href="{{route('game',['id'=>$game_open->id, 'status'=>'join'])}}" target="_blank"><i class="fa fa-3x fa-sign-in"></i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-primary" id="playingGamesPanel">
                <div class="panel-heading">
                    <h3 class="panel-title">Watch Game</h3>
                </div>
                <div class="panel-body">
                    <table id="playingGames" class="display" width="100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Max Players</th>
                            <th>Size</th>
                            <th>Created At</th>
                            <th>Admin</th>
                            <th>Enter</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($games_running as $game_running)
                            <tr>
                                <td>{{ $game_running->name }}</td>
                                <td>{{ $game_running->num_max_players }}</td>
                                <td>{{ $game_running->cols }} x {{ $game_running->lines }}</td>
                                <td>{{ $game_running->created_at }}</td>
                                <td></td>
                                <td><a href="{{route('game',['id'=>$game_running->id, 'status'=>'watch'])}}" target="_blank"><i class="fa fa-3x fa-eye"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <div class="col-md-4">

            <div class="panel panel-primary" id="topTenPanel">
                <div class="panel-heading">
                    <h3 class="panel-title">Top Ten</h3>
                </div>
                <div class="panel-body">
                    @foreach($top_ten as $wins)
                        <span>{{ $wins->nickname }} : {{ $wins->tot_wins }} @if($wins->tot_wins == 1) victory @else victories @endif</span>
                    @endforeach
                </div>
            </div>

            <div class="panel panel-primary" id="chatPanel">
                <div class="panel-heading">
                    <h3 class="panel-title">Chat</h3>
                </div>
                <div class="panel-body">
                    <input type="hidden" id="playerName" name="playerName" value="{{ Auth::user()->nickname }}">
                    <div id="messages">
                        <span ng-repeat="m in chatMessages track by $index">@{{ m }}</span>
                    </div>
                    <form action="#" method="get" id="id_form_chat">
                        <input class="form-control" id="m" autocomplete="off"  ng-model="chatMsg" ng-keypress="keyPressMsg($event)">
                    </form>
                </div>
            </div>

        </div>

    </div>
</div>
@stop