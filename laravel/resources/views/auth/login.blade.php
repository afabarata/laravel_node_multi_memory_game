@extends('layout')

@section('css')
    <link rel="stylesheet" href="{{ URL::asset('assets/css/styles.css')}}">
@stop

@section('content')
<div class="container" id="login">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <form method="post" action="/auth/login">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Login Form - Authentication</h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <label for="nickname">Nickname</label>
                        <input type="text" class="form-control" name="nickname" id="nickname" placeholder="Nickname" value="{{ old('nickname') }}" >
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <hr>
                   <div class="text-center">
                       <a class="btn btn-primary btn-social btn-mg btn-facebook pull-left">
                           <span class="fa fa-facebook"> | </span>
                           Login - Facebook
                       </a>
                       <a class="btn btn-info btn-social btn-twitter">
                           <span class="fa fa-twitter"> | </span>
                           Login - Twitter"
                       </a>
                       <a class="btn btn-danger btn-social btn-google pull-right">
                           <span class="fa fa-google"> | </span>
                           Login - Google
                       </a>
                   </div>
                </div>
                <div class="panel-footer">
                    <div class="row footerBtn">
                        <div class="pull-left">
                            <a type="button" href="{{route('home')}}"class="btn btn-danger">Cancel</a>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success">Login</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-3"></div>
</div>
@stop
