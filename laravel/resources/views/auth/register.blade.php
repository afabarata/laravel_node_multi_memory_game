@extends('layout')

@section('css')
    <link rel="stylesheet" href="{{ URL::asset('assets/css/styles.css')}}">
@stop

@section('content')
<div class="container" id="register">
    <div class="col-md-6 col-md-offset-3">
        <form method="post" action="/auth/register">
            {!! csrf_field() !!}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Registration Form</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="nickname">Nickname</label>
                        <input type="text" class="form-control" name="nickname" id="nickname" placeholder="Nickname" value="{{ old('nickname') }}" >
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{ old('email') }}" >
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Password Confirmation</label>
                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Password">
                    </div>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif


                </div>
                <div class="panel-footer">
                    <div class="row footerBtn">
                        <div class="pull-left">
                            <a type="button" href="{{route('home')}}"class="btn btn-danger">Cancel</a>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success">Register</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop