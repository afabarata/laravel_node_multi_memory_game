<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App;
use App\Game;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class PagesController extends Controller
{

    public function gameLobby(Request $request) {

        $user_id = Auth::user()->id;
        $id = $request->get('id');

        if($id) {
            $game = Game::find($id);
            if($user_id == $game->id_user) {
                $game->status = 2;
                $game->save();
            }
        }

        $games_open = Game::where('status','0,1')->where('private','0')->latest()->get();

        $games_running = Game::where('status','0,1')->where('private','0')->latest()->get();

        $top_ten = DB::table('top_ten')->select(DB::raw('nickname, count(*) as tot_wins'))->groupBy('nickname')->take(10)->orderBy('tot_wins', 'desc')->get();

        return view ('pages.gameLobby')->with([
            'games_open' => $games_open,
            'games_running' => $games_running,
            'top_ten' => $top_ten
        ]);
    }

}
