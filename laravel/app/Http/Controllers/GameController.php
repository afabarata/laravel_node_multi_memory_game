<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Game;
use App\TopTen;
use App\Http\Requests;
use App\Http\Controllers\Auth;
use App\Http\Requests\CreateGameRequest;

class GameController extends Controller
{

    public function getGameCreation() {

        return view ('games.gameCreation');
    }

    public function postGameCreation(CreateGameRequest $request)
    {
        $game = Game::create($request->all());

        //saber qual o ultimo id inserido
        $id= $game->id;

        return redirect()->route('game',$id);
    }

    public function game(Request $request, $id) {

        $status = $request->get('status');

        $game = Game::find($id);

        return view ('games.game')->with('game',$game)->with('status',$status);
    }

    public function changeStatus(Request $request) {

    }

    public function postGameOver(Request $request)
    {
        TopTen::create($request->all());

        $game_id = $request->get('gameId');

        if($game_id) {
            $game = Game::find($game_id);
            $game->status = 2;
            $game->save();
        }

        return redirect()->route('gameLobby');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
