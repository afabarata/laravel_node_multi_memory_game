<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* Views */

Route::get('/', ['as' => 'home','uses' =>function () {
    return view('welcome');
}]);

Route::get('/gameLobby', ['as' => 'gameLobby', 'middleware' => 'auth', 'uses' => 'PagesController@gameLobby']);

Route::get('/gameCreation', ['as' => 'gameCreation', 'middleware' => 'auth', 'uses' => 'GameController@getGameCreation']);
Route::post('/gameCreation','GameController@postGameCreation');

Route::get('/game/{id}', ['as' => 'game', 'middleware' => 'auth', 'uses' => 'GameController@game']);
Route::post('/game','GameController@postGameOver');

/* Authentication routes */
Route::get('auth/login', ['as' => 'login','uses' =>'Auth\Authcontroller@getLogin']);
Route::post('auth/login', ['as' => 'login','uses' =>'Auth\Authcontroller@postLogin']);

Route::get('auth/logout', ['as' => 'logout','uses' =>'Auth\Authcontroller@getLogout']);

/* Registation routes */
Route::get('auth/register', ['as' => 'register','uses' =>'Auth\Authcontroller@getRegister']);
Route::post('auth/register', ['as' => 'register','uses' =>'Auth\Authcontroller@postRegister']);

/* Forgot Password */
Route::controllers([
   'password' => 'Auth\PasswordController',
]);


