<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopTen extends Model
{
    /**
     * The database table high_score by the model.
     *
     * @var string
     */
    protected $table = 'top_ten';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'nickname', 'pairs', 'created_at' ];
}