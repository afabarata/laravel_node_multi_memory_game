(function(){
    "use strict";

    function GameController ($scope) {

        var gameId = $("#idGame").val();
        var playerId = $("#idPlayer").val();
        var playerName = $("#playerName").val();
        var numCols = $("#cols").val();
        var numLines = $("#lines").val();
        var maxPlayers = $("#maxPlayers").val();

        $scope.tiles = [];
        $scope.current_players = 0;
        $scope.currentPlayerId = 0;
        $scope.currentPlayerName = "";
        $scope.playerList = [];
        $scope.winner = [];

        $scope.chatMsg = "";
        $scope.chatAll = false;
        $scope.chatMessages = [];

        console.log('connect');

        var protocol = location.protocol;
        var port = '8080';
        var url = protocol + '//' + window.location.hostname + ':' + port;

        console.log(url);

        var socket = io.connect(url, {reconnect: true});

        $scope.startGame = function() {
            console.log('startGame');
            socket.emit("startGame", playerId,playerName,gameId,numCols,numLines,maxPlayers);
        };

        $scope.joinGame = function() {
            console.log('joinGame');
            socket.emit("joinGame", playerId,playerName,gameId);
        };

        $scope.watchGame = function() {
            console.log('watchGame');
            socket.emit("watchGame", playerId,gameId);
        };

        $scope.touch = function(tile){

            if(!tile.flipped) {

                tile.flipped = true;

                setTimeout(function () {
                    console.log('Player move: ', playerId);
                    socket.emit("playMove", playerId, gameId, $scope.tiles );
                }, 2000);

            }
        };

        socket.on('refreshGame', function(data, playersList, player, playerCounter, status){
            console.log('RefreshGame', data);
            $scope.tiles = data;
            $scope.currentPlayerId = player.id;
            $scope.currentPlayerName = player.nickname;
            $scope.current_players = playerCounter;
            $scope.playerList = playersList;
            $scope.$apply();

            if(status){

                for(var i = 0; i < playerCounter; i++) {

                    if($scope.winner.length == 0) {
                        $scope.winner = playersList[i];
                    } else {
                        if($scope.winner.pairs <= playersList[i].pairs) {
                            if($scope.winner.pairs = playersList[i].pairs) {
                                if($scope.winner.timer > playersList[i].timer){
                                    $scope.winner = playersList[i];
                                }
                            } else {
                                $scope.winner = playersList[i];
                            }
                        }
                    }
                }

                alertify.minimalDialog || alertify.dialog('minimalDialog',function(){
                    return {
                        main:function(content){
                            this.setContent(content);
                        },
                        setup:function(){
                            return {
                                focus:{
                                    element:function(){
                                        return this.elements.body.querySelector(this.get('selector'));
                                    },
                                    select:true
                                },
                                options:{
                                    title: "<span style='font-size:1.25em;'>The Game is over!</span>",
                                    //closable: false,
                                    maximizable: false,
                                    resizable: false,
                                    padding: true,
                                    movable: false
                                }
                            };
                        }
                    };
                });

                alertify.minimalDialog ($('#finalGameContent')[0]).set('selector');
            }
        });

        $scope.keyPressMsg = function($event){
            if ($event.keyCode == 13) {
                console.log('msgChat to Game Only', gameId, playerName, $scope.chatMsg);
                socket.emit("msgChatGame", gameId, playerName, $scope.chatMsg);
                $scope.chatMsg = "";
            }
        };

        socket.on('newChatMsg', function(playerName, newMsg){
            console.log('newChatMsg', playerName, newMsg);
            if ($scope.chatMessages.length>10)
                $scope.chatMessages.shift();
            $scope.chatMessages.push(playerName + ": " +newMsg);
            console.log($scope.chatMessages);
            $scope.$apply();
        });


    }

    angular.module('memoryGame', ['rzModule', 'angular-flippy']);
    angular.module('memoryGame').controller('GameController', ['$scope',GameController]);

})();