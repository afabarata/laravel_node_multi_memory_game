$( document ).ready(function() {

    var currentTiles;
    var maxTiles = 80;

    $("#privateYes").click(function() {
        $("#maxPlayer").attr('hidden',true);
        $("#num_max_players").val("1");
    });

    $("#privateNo").click(function() {
        $("#maxPlayer").removeAttr('hidden');
        $("#num_max_players").val("");
    });


    $( "#cols" ).val("2");
    $( "#lines" ).val("2");

    $(function() {

        $("#sliderCols").slider({
            value:2,
            min: 2,
            max: 10,
            slide: function( event, ui ) {
                $( "#cols" ).val(ui.value);

                currentTiles = ( ui.value * $('#sliderLines').slider("option", "value") );

                if(currentTiles > maxTiles) {
                    $('#sliderLines').slider("value", $('#sliderLines').slider("option", "value") - Math.abs($('#sliderLines').slider("option", "value") - ui.value));
                }
                if(currentTiles % 2 != 0){
                    $('#sliderLines').slider("value", $('#sliderLines').slider("option", "value") - 1);
                }

                $( "#lines" ).val($('#sliderLines').slider("option", "value"));
            }
        });

        $( "#sliderLines" ).slider({
            value:2,
            min: 2,
            max: 10,
            slide: function( event, ui ) {
                $( "#lines" ).val(ui.value);

                currentTiles = ( ui.value * $('#sliderCols').slider("option", "value") );

                if(currentTiles > maxTiles) {
                    $('#sliderCols').slider("value", $('#sliderCols').slider("option", "value") - Math.abs($('#sliderCols').slider("option", "value") - ui.value));
                }
                if(currentTiles % 2 != 0){
                    $('#sliderCols').slider("value", $('#sliderCols').slider("option", "value") - 1);
                }

                $( "#cols" ).val($('#sliderCols').slider("option", "value"));
            }
        });

    });

});