$( document ).ready(function() {

    $('#openGames').DataTable({
        responsive: true
    });

    $('#playingGames').DataTable({
        responsive: true
    });

    setInterval(function () {
        location.reload();
    }, 5000);

});

(function(){
    "use strict";

    function LobbyController ($scope,$http,$log,$timeout) {

        $scope.playerName = $("#playerName").val();
        $scope.chatMsg = "";
        $scope.chatMessages = [];

        console.log('connect');

        var protocol = location.protocol;
        var port = '8080';
        var url = protocol + '//' + window.location.hostname + ':' + port;

        console.log(url);

        var socket = io.connect(url, {reconnect: true});

        // CHAT - Simple Version
        $scope.keyPressMsg = function($event){
            if ($event.keyCode == 13) {
                console.log('msgChat to All ', $scope.playerName, $scope.chatMsg);
                socket.emit("msgChatAll", $scope.playerName, $scope.chatMsg);
                $scope.chatMsg = "";
            }
        };

        socket.on('newChatMsg', function(playerName, newMsg){
            console.log('newChatMsg', playerName, newMsg);
            if ($scope.chatMessages.length>10)
                $scope.chatMessages.shift();
            $scope.chatMessages.push(playerName + ": " +newMsg);
            console.log($scope.chatMessages);
            $scope.$apply();
        });



    }

    angular.module('memoryGame', []);
    angular.module('memoryGame').controller('LobbyController', ['$scope','$http','$log','$timeout',LobbyController]);

})();