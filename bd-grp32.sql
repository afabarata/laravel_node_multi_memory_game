-- Adminer 4.2.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `games`;
CREATE TABLE `games` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `num_max_players` int(11) NOT NULL,
  `current_num_players` int(11) NOT NULL DEFAULT '0',
  `cols` int(11) NOT NULL,
  `lines` int(11) NOT NULL,
  `private` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table',	1),
('2014_10_12_100000_create_password_resets_table',	1),
('2015_12_25_000000_create_games_table',	1),
('2015_12_25_000000_create_top_ten_table',	1)
ON DUPLICATE KEY UPDATE `migration` = VALUES(`migration`), `batch` = VALUES(`batch`);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `top_ten`;
CREATE TABLE `top_ten` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pairs` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `top_ten` (`id`, `nickname`, `pairs`, `created_at`, `updated_at`) VALUES
(1,	'PauloPenicheiro',	2,	'2016-01-04 16:29:05',	'2016-01-04 16:29:05'),
(2,	'AndreBarata',	2,	'2016-01-04 16:33:29',	'2016-01-04 16:33:29'),
(3,	'PauloPenicheiro',	3,	'2016-01-04 16:41:31',	'2016-01-04 16:41:31')
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `nickname` = VALUES(`nickname`), `pairs` = VALUES(`pairs`), `created_at` = VALUES(`created_at`), `updated_at` = VALUES(`updated_at`);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_nickname_unique` (`nickname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` (`id`, `nickname`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'AndreBarata',	'2130869@my.ipleiria.pt',	'$2y$10$709P/BBEiFWEU9J85CZIs.dRbBjK36U4THuNN8e.9Z2WStYYVIA8y',	'WETNJLXpRxn4vwb335wsARh1lG1mPh9L55EctgQeOHP2p9Y9Y4ZHhcRPkdzZ',	'2016-01-04 15:46:59',	'2016-01-04 16:54:58'),
(2,	'PauloPenicheiro',	'2130869@my.ipleiria.pt',	'$2y$10$YE3KAZKWIryImuT8F7uvLOjChqM9QYDbgJ54C5JDvLL5TQ1u.tA0G',	NULL,	'2016-01-04 15:47:12',	'2016-01-04 15:47:12')
ON DUPLICATE KEY UPDATE `id` = VALUES(`id`), `nickname` = VALUES(`nickname`), `email` = VALUES(`email`), `password` = VALUES(`password`), `remember_token` = VALUES(`remember_token`), `created_at` = VALUES(`created_at`), `updated_at` = VALUES(`updated_at`);

-- 2016-01-04 16:58:37